var mousePosition = {x: null, y: null}; //Peles koordinati
var clickPosition = {x: null, y: null}; //Klik koordinates

//Dabujam peles koordinatu un korektejam to atbilstosi canvas izmeram
function getMousePos(canvas, evt) {
    return {
      x: evt.x - canvas.offsetLeft,
      y: evt.y - canvas.offsetTop
    };
}

//Asynhroni apstradam klikus
async function systemClick(){
    //Ja nav jauns notikums, tad drikst stradat ar sistemam un pariet uz nakamo gajienu
    if(!msg.show){
        for (var i = 0; i < clickebleElements.length; i++) {
            if( (clickPosition.x > clickebleElements[i].x && clickPosition.x < clickebleElements[i].x + 38) && (clickPosition.y > clickebleElements[i].y && clickPosition.y < clickebleElements[i].y + 38) ) {    
                let otherClosed = true;
                for(var ii = 0; ii < clickebleElements.length; ii++)
                    if(clickebleElements[ii].systemOpened){
                        otherClosed = false;
                        break;
                    }
                clickebleElements[i].systemOpened = (otherClosed) ? true : false;
            }
    
            if(clickPosition.x > 712 && clickPosition.x < 776 && clickPosition.y > 297 && clickPosition.y < 362 && clickebleElements[i].systemOpened)
                clickebleElements[i].systemOpened = false;
    
            if(clickPosition.x > 9 && clickPosition.x < 70 && clickPosition.y > 7 && clickPosition.y < 67 && !clickebleElements[i].systemOpened)
                musicOn = !musicOn;
        }
    
        if(clickPosition.x > 665 && clickPosition.x < 793 && clickPosition.y > 446 && clickPosition.y < 684)
            nextTurn();
    }
   
}

//Asynhroni mainam bilde elementiem uz kuriem rada kursors
async function systemHover(){
    for (var i = 0; i < clickebleElements.length; i++) {
        if( (mousePosition.x > clickebleElements[i].x && mousePosition.x < clickebleElements[i].x + 38) && (mousePosition.y > clickebleElements[i].y && mousePosition.y < clickebleElements[i].y + 38) )
            clickebleElements[i].sprite.src = clickebleElements[i].src_hover;
        else clickebleElements[i].sprite.src = (clickebleElements[i].system.isTurned) ? clickebleElements[i].src_turned : clickebleElements[i].src;
    }

    if(mousePosition.x > 665 && mousePosition.x < 793 && mousePosition.y > 446 && mousePosition.y < 684)
        ctx.drawImage(next_turn, 0, 0);

    if(mousePosition.x > 9 && mousePosition.x < 70 && mousePosition.y > 7 && mousePosition.y < 67)
        if(musicOn)
            music.src = music_on_hover;
        else music.src = music_off_hover;
    else if(musicOn)
        music.src = music_on;
        else music.src = music_off;

}