class System{

    //Definejam kas par sistemu vispar ir
    constructor(SystemName, Status, Text, IsTurned, Name){
        
        this.systemName = SystemName;
        this.status = Status;
        this.text = Text;
        this.isTurned = IsTurned;

        this.back = new Image();
        this.back.src = msg_box_src;

        this.switch = new Image();
        this.close = new Image();

        this.myName = Name;
    
    }

    //Metode lai zimet un apstradat to darbibu
    showMSG(){
        ctx.drawImage(this.back, 0, 0);

        //Mainam teksturu ja vajag
        if(this.isTurned)
            this.switch.src = switch_on_src;
        else this.switch.src = switch_off_src;
        ctx.drawImage(this.switch, 0, 0);

        //Hover effekts
        if(mousePosition.x > 712 && mousePosition.x < 776 && mousePosition.y > 297 && mousePosition.y < 362)
            this.close.src = close_src_hover;
        else this.close.src = close_src;

        //Ja systemai ir stavoklis 0, tad vina nevar stradat
        if(this.status <= 0)
            this.isTurned = false;

            //Ja cilveks grib ieslegt sistemu un vinam ir energija un stavoklis sitemai ir vairak par 10, tad iesledzam, otradi rakstam zinu ka nedrikst ieslegt
        if(clickPosition.x > 673 && clickPosition.x < 747 && clickPosition.y > 22 && clickPosition.y < 101 && this.status > 0)
            if(energy > 0 || this.isTurned){
                this.isTurned = !this.isTurned;
                if(this.isTurned)
                    energy--;
                else energy++;
            }
            else{
                ctx.font = "40px Gamja Flower";
                ctx.fillStyle = "#F00";
                ctx.fillText(noEnergy, 70, 350);
                miliseconds += 500;

                ctx.font = "20px Gamja Flower";
                ctx.fillStyle = "#0F0";
            }


        ctx.drawImage(this.close, 0, 0);

        //Izvadam visu informaciju par sistemu
        ctx.fillText(systemTxt + this.systemName, 100, 70);
        ctx.fillText(statusTxt + this.status + "%", 100, 100);
        wrapText(this.text, 100, 160, 600, 25);
    }

    //Dod iespeju erti mainit stavokli
    setTurned(boolean){
        this.isTurned = boolean;
    }

}