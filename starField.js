var star_canv = document.getElementById("Stars");
var star_ctx = star_canv.getContext("2d");

star_canv.width = window.innerWidth;
star_canv.height = window.innerHeight;

star_ctx.strokeStyle = 'rgba(200,200,250,'+ (Math.random() * (1.0 - 0.3) + 0.3) +')';
star_ctx.lineWidth = 1;
star_ctx.lineCap = 'round';

var particles = [];
var MaxParticles = 500;

initilization();
draw();

//Katrai zveigzinetei ir savs atrums virziens un sakum koordites
function initilization(){

    for(var i = 0; i < MaxParticles; i++){
        particles.push({
            x: Math.random() * star_canv.width,
            y: Math.random() * star_canv.height,
            speed_x: Math.random() * 5 + 1,
            speed_y: -4 + Math.random() * 5 + 1
        });
    }
}

//Paraleli spelei zimejam un rekinam nakamo pozicitiju zvaignem
//Ta ir rekursiva funkcija
async function draw(){
    star_ctx.clearRect(0, 0, star_canv.width, star_canv.height);

    for(var i = 0; i < MaxParticles; i++) {
        if(particles[i].x <= star_canv.width) {
            particles[i].x += particles[i].speed_x / 20;
            particles[i].y += particles[i].speed_y / 20;
        }
        else {
            particles[i].x = 0;
            particles[i].y = Math.random() * star_canv.height;
        }
        
    }

    for(var i = 0; i < MaxParticles; i++){
        star_ctx.beginPath();
        star_ctx.rect(particles[i].x, particles[i].y, 1, 1);
        star_ctx.stroke();
    }

    requestAnimationFrame(draw);
}

