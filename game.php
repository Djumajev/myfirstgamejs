<!DOCTYPE html>
<head>

    <meta name="viewport" content="width=1287">
    <title>Space Crew</title>

    <link rel="stylesheet" href="./style/style.css">
    <link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">

</head>

<body id="body">

<div style = "background-color: #000; width: 100vw; height: 100vh; z-index: 100; position: fixed; top: 0;" id=loadingScreen>
    <img src="./images/loading.gif" style="margin: auto; right: 0; position: fixed;"> <!-- https://loading.io/#_=_ -->
        <h1 id=msg>Поверните телефон!</h1>
    <h1 style="color: #FFF; width: 50%; margin: auto; position: fixed; bottom: 20%; left: 40%; ">Loading...</h1>
</div>

<stars>
    <canvas id="Stars" width=100vw height=100vh>Error :( </canvas>
</stars>


<game>
    <canvas id="MainScene" width=800px height=600px>Error :(</canvas>
</game>

<h1 id=msg>Поверните телефон!</h1>

<?php

    function insert_base64_encoded($img, $name){
        $imageSize = getimagesize($img);
        $imageData = base64_encode(file_get_contents($img));
        $imageSrc = "<div style=\"display: none;\" id='$name'> data:{$imageSize['mime']};base64,{$imageData} </div>";
        echo $imageSrc;
    }

    $backs = array();

    foreach (glob("./images/nebula/*.png") as $filename)
    {
        array_push($backs, $filename);
    }

    $rand_key = array_rand($backs, 1);
    insert_base64_encoded( $backs[$rand_key], "back");

    foreach (glob("./images/*.png") as $filename)
    {
        insert_base64_encoded( $filename, $filename);
    }

?>

<script src="sprites.js"></script>
<script src="messages.js"></script>
<script src="system.js"></script>
<script src="scenario.js"></script>
<script src="audio.js"></script>
<script src="starField.js"></script>
<script src="inputs.js"></script>
<script src="game.js"></script>



</body>