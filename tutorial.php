<!DOCTYPE html>
<head>

    <meta name="viewport" content="width=1287">
    <title>Space Crew</title>

    <link rel="stylesheet" href="./style/style.css">
    <link href="https://fonts.googleapis.com/css?family=Gamja+Flower" rel="stylesheet">

</head>

<body id="body">

<div style = "background-color: #000; width: 100vw; height: 100vh; z-index: 100; position: fixed; top: 0;" id=loadingScreen>
    <img src="./images/loading.gif" style="margin: auto; right: 0; position: fixed;"> <!-- https://loading.io/#_=_ -->
        <h1 id=msg>Поверните телефон!</h1>
    <h1 style="color: #FFF; width: 50%; margin: auto; position: fixed; bottom: 20%; left: 40%; ">Loading...</h1>
</div>

<?php

    function insert_base64_encoded($img, $name){
        $imageSize = getimagesize($img);
        $imageData = base64_encode(file_get_contents($img));
        $imageSrc = "<div style=\"display: none;\" id='$name'> data:{$imageSize['mime']};base64,{$imageData} </div>";
        echo $imageSrc;
    }

    $backs = array();

    foreach (glob("./images/nebula/*.png") as $filename)
    {
        array_push($backs, $filename);
    }

    $rand_key = array_rand($backs, 1);
    insert_base64_encoded( $backs[$rand_key], "back");

?>

<stars>
    <canvas id="Stars" width=100vw height=100vh>Error :( </canvas>
</stars>

<h1 id=msg>Поверните телефон!</h1>

<div style="width: 50%; margin: auto; color: #FFF; text-align: center; margin-top: 10%; background-color: #00FFAA11; padding: 2%; border-radius: 0%;">
    <h2>Обучение игре</h2>

    <p>
        Добро пожаловать в академию, кадет! <br> <br>
        Здесь ты научишься управлять космическим кораблём и станешь настоящим профессианолом по питью чая.<br>
        Давай начнём с простого. Это твой корабль.
        <img src="./Tutorial/dictor 5.png" width=100%>
        Для пущего пафоса и уважения в лице остального экипажа, можешь включить музыку в левом вверхем углу.
        <img src="./Tutorial/dictor 8.png" width=100%>
        На корабле есть системы. Каждая система нужна для конретнных задач, можешь смело нажимать на них, что бы получить подробное описание системы и узнать её текущее состояние.
        Для включения системы надо нажать на большой красный включатель, если всё удачно - он станет зелёным. Нельзя включать системы с состоянием 0% и для запитки системы требуется 1 единица энергии.
        Включённая система за ход теряет 10% своего состояния, потому что твой экипаж - дипломированые макаки.
        <img src="./Tutorial/dictor 10.png" width=100%> 
        <img src="./Tutorial/dictor 12.png" width=100%>
        Некоторые системы за ход восполняют твои ресурсы, а так же любая система может быть необходима включенной в случайном событии на следующем ходу. Собственно твои ресурсы:
        <ul style="text-align: left;">
        <li>Энергия - необходима для включения систем<br><label style="font-size: 80%;">(Если реактор ломается - энергия пропадает)</label></li>
        <li>Химикаты и металлы - могут пригодится в совершенно непредсказуемых ситуациях</li>
        <li>Здоровие экипажа - самый главный ресурс, когда он закончиться, ты проиграешь</li>
        </ul> 
        <img src="./Tutorial/dictor 17.png" width=100%>
        Когда подсчёт провизии, включение систем и крушка чая готовы, нажимай на кнопку "следующий ход"
        <img src="./Tutorial/dictor 19.png" width=100%>
        В начале хода происходит одно СЛУЧАЙНОЕ событие, которое может как положительно отразиться на полёте, так и убить весь экипаж. Событие состоит из самого события и вариантов её решения, которые выбираешь ты.
        В скобках пишется условие решения (если таковое имеется) и при соблюдении этих условий, ты можешь выбрать этот вариант развития твоего приключения.
        <img src="./Tutorial/dictor 26.png" width=100%>
        Игра продолжается до тех пор, пока живёт твоя команда, целью игры считается набрать максимально большой рекорд(максимально много сделать ходов), после смерти твоего экипажа ты можещь опубликавать свой результат
        и если он хорош, то поподёшь на таблицу лучших игроков.<br>
        <h3>Удачи!</h3>
    </p>

    <form action="index.php">
        <button><span style=\"font-size: 80%;\">Домой</span></button>
    </form>
    
</div>  

<script>

  var back = document.getElementById("back").innerHTML;

</script>

<script src="starField.js"></script>

<script>
    document.addEventListener(
        "DOMContentLoaded",
        function(){
            document.getElementById("loadingScreen").style.display = "none";
            document.body.style.background = "url(" + back + ")";
            document.body.style.backgroundAttachment = "fixed";
        }
    );
</script>

</body>