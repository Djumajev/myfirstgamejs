//Globals tekst kurs tik izmantots spele
var noEnergy = "Nepietiek enerģijas!",
systemTxt = "Sistēma: ",
systemTxtW = "Sistēma ",
conditionFalse = "Nosacījumi nav izpildīti",
statusTxt = "Stāvoklis: ",
turnedOn = ": ON; ",
metalTxt = "Metāls: ",
healthTxt = "Veselība: ",
chemicalsTxt = "Ķīmiskās vielas: ";

//Noteikumi kuri var notikt ar speleteju
//Svarigi ka pirmais - pitmais kuru cilveks redzes
//Pedejais - kad cilveks zaude
//Ja nevajag radit resursu rakstam "", ja bus tuks, var gaidit kludu
//txt massiva rakstam tekstu izvelem
//req massiva kas nepieciesams
//Svarigi! ja txt massiva tekst bus 1 vieta, kas nepiecisams ari bus 1 vieta massiva req
//Ludzu rakstitet tik daudz req, cik ir txt massiva elementus
//Drikst izmantot "" tuksas iekavas
var scenario = [
    {
        txt: "Laipni lūdzam, kapteinis. Noklikšķiniet tālāk, lai turpinātu.",
        requrment: {
            txt: ["Noklikšķiniet uz mani"],
            req: [
                {system: "", metal: "", colonist: "", chemicals: "", repair: {system: "", procents: ""} }
            ]
        }
    },

    {
        txt: "Netālu no gāzes giganta atrodat veco satelītu.",
        requrment: {
            txt: ["Nedariet neko", "Izjaukt uz detaļām", "Izmantot kā komutatoru"],
            req: [
                {system: "", metal: "", colonist: "", chemicals: "", repair: {system: "", procents: ""} },
                {system: "C", metal: "1", colonist: "", chemicals: "", repair: {system: "", procents: ""} },
                {system: "A", metal: "", colonist: "1", chemicals: "", repair: {system: "", procents: ""} }
            ]
        }
    },

    {
        txt: "Jūsu radars neuztvēra asteroīdu, kas steidzas uz jūms.",
        requrment: {
            txt: ["Nedariet neko", "Izvairīties", "Izvairīties", "Pārstrādā"],
            req: [
                {system: "", metal: "", colonist: "-1", chemicals: "", repair: {system: "", procents: ""} },
                {system: "G", metal: "", colonist: "", chemicals: "", repair: {system: "", procents: ""} },
                {system: "F", metal: "", colonist: "", chemicals: "", repair: {system: "", procents: ""} },
                {system: "C", metal: "1", colonist: "", chemicals: "", repair: {system: "C", procents: "-10"} }
            ]
        }
    },

    {
        txt: "Jūs nokļūstat saules starojumā.",
        requrment: {
            txt: ["Nedariet neko", "Ātri atstāj zonu"],
            req: [
                {system: "", metal: "", colonist: "-1", chemicals: "", repair: {system: "", procents: ""} },
                {system: "E", metal: "", colonist: "", chemicals: "", repair: {system: "E", procents: "-10"} }
            ]
        }
    },


    {
        txt: "Jūsu kuģis lēni lido līdz stacijai MK-1298. Spriežot pēc arhīva, tas jau sen ir pamesti, un jūs nolemjat izjaukt to, lai noremontēt ...",
        requrment: {
            txt: ["Lidot tālāk", "Fiksēt reaktoru", "Fiksēt gaisa filtru", "Fiksēt kreiso dzinēju", "Nekas"],
            req: [
                {system: "", metal: "", colonist: "", chemicals: "", repair: {system: "", procents: ""} },
                {system: "C", metal: "", colonist: "", chemicals: "", repair: {system: "E", procents: "10"} },
                {system: "C", metal: "", colonist: "", chemicals: "", repair: {system: "B", procents: "10"} },
                {system: "C", metal: "", colonist: "", chemicals: "", repair: {system: "G", procents: "10"} },
                {system: "C", metal: "1", colonist: "", chemicals: "", repair: {system: "", procents: ""} }
            ]
        }
    },

    {
        txt: "Šajā sistēmā nav absolūti nekas, jūs sākat dzinējus un gatavojieties kibernoziegumam.",
        requrment: {
            txt: ["Lidot prom"],
            req: [
                {system: "", metal: "", colonist: "", chemicals: "", repair: {system: "", procents: ""} }
            ]
        }
    },

    {
        txt: "Sensori pamanīja planētu ar piemērotu atmosfēru.",
        requrment: {
            txt: ["Lidot tālāk", "Meklēt noderīgus resursus", "Meklēt noderīgus resursus"],
            req: [
                {system: "", metal: "", colonist: "", chemicals: "", repair: {system: "", procents: ""} },
                {system: "G", metal: "1", colonist: "", chemicals: "1", repair: {system: "", procents: ""} },
                {system: "F", metal: "1", colonist: "", chemicals: "1", repair: {system: "", procents: ""} }
            ]
        }
    },

    {
        txt: "Viens no jūsu apkalpes locekļiem izgāja kosmosā, lai apkalpot apparaturu, bet viņa kabelis lauza.",
        requrment: {
            txt: ["Saglabāt", "Atstājiet"],
            req: [
                {system: "E", metal: "", colonist: "", chemicals: "", repair: {system: "A", procents: "10"} },
                {system: "", metal: "", colonist: "-1", chemicals: "", repair: {system: "", procents: ""} }
            ]
        }
    },

    {
        txt: "Ēdamistabā galds izputējis, apkalpes loceklis sāka nemierināties.",
        requrment: {
            txt: ["Izveidojiet jaunu galdu", "Izmetiet nemiernieku līderi", "Izveidojiet mucu, kas visus padarītu slimu", "Izveidojiet mirušu cilpu, kas visiem padarītu slimu"],
            req: [
                {system: "", metal: "", colonist: "", chemicals: "-1", repair: {system: "", procents: ""} },
                {system: "", metal: "", colonist: "-1", chemicals: "", repair: {system: "", procents: ""} },
                {system: "G", metal: "", colonist: "", chemicals: "", repair: {system: "", procents: ""} },
                {system: "F", metal: "", colonist: "", chemicals: "", repair: {system: "", procents: ""} }
            ]
        }
    },

    {
        txt: "Jonas vētra sistēmā apdraud visu elektroniku uz kuģa.",
        requrment: {
            txt: ["Izlidojiet prom, cik drīz vien iespējams", "Izlidojiet prom, cik drīz vien iespējams", "Nedariet neko"],
            req: [
                {system: "F", metal: "", colonist: "", chemicals: "", repair: {system: "", procents: ""} },
                {system: "G", metal: "", colonist: "", chemicals: "", repair: {system: "", procents: ""} },
                {system: "", metal: "", colonist: "-1", chemicals: "", repair: {system: "", procents: ""} }
            ]
        }
    },

    {
        txt: "Hidroponiskas saimniecības nav kārtībā, cilvēki badā.",
        requrment: {
            txt: ["Lai badā", "Sintezēt pārtiku", "Labot saimniecību"],
            req: [
                {system: "", metal: "", colonist: "-1", chemicals: "", repair: {system: "", procents: ""} },
                {system: "", metal: "", colonist: "", chemicals: "-1", repair: {system: "", procents: ""} },
                {system: "C", metal: "", colonist: "", chemicals: "", repair: {system: "", procents: ""} }
            ]
        }
    },

    {
        txt: "Inženieri piedāvā noslēgt caurules ar līmlenti.",
        requrment: {
            txt: ["Piekrītu", "Atmest"],
            req: [
                {system: "", metal: "", colonist: "", chemicals: "-1", repair: {system: "G", procents: "10"} },
                {system: "", metal: "", colonist: "", chemicals: "", repair: {system: "", procents: ""} }
            ]
        }
    },

    {
        txt: "Sistēmā ir reti sastopama anomālija, kas, mijiedarbojoties ar metāliem, reproducē enerģiju. Tas varētu palīdzēt mums likvidēt reaktoru remontam, nezaudējot elektroenerģiju.",
        requrment: {
            txt: ["Nedariet neko", "Izmetiet metālu"],
            req: [
                {system: "", metal: "", colonist: "", chemicals: "", repair: {system: "", procents: ""} },
                {system: "", metal: "-1", colonist: "", chemicals: "", repair: {system: "E", procents: "10"} }
            ]
        }
    },

    {
        txt: "Apkalpes darbinieki atlaida idejas, lai kaut ko labotu.",
        requrment: {
            txt: ["Labot tiltu", "Labot medicīnisko zonu", "Labot filtri", "Nedariet neko"],
            req: [
                {system: "", metal: "-1", colonist: "", chemicals: "", repair: {system: "A", procents: "10"} },
                {system: "", metal: "", colonist: "", chemicals: "-1", repair: {system: "D", procents: "10"} },
                {system: "", metal: "-1", colonist: "", chemicals: "-1", repair: {system: "B", procents: "10"} },
                {system: "", metal: "", colonist: "", chemicals: "", repair: {system: "", procents: ""} }
            ]
        }
    },

    {
        txt: "Apkalpes darbinieki atlaida idejas, lai kaut ko labotu.",
        requrment: {
            txt: ["Labot kreiso dzinēju", "Labot labo dzinēju", "Reaktoru", "Nedariet neko"],
            req: [
                {system: "", metal: "-1", colonist: "", chemicals: "", repair: {system: "G", procents: "10"} },
                {system: "", metal: "", colonist: "", chemicals: "-1", repair: {system: "F", procents: "10"} },
                {system: "", metal: "-1", colonist: "", chemicals: "-1", repair: {system: "E", procents: "10"} },
                {system: "", metal: "", colonist: "", chemicals: "", repair: {system: "", procents: ""} }
            ]
        }
    },

    {
        txt: "Ļoti karsta zvaigzne sistēmā ļauj mums izkausēt dažus no mūsu resursiem, lai atjauninātu reaktora pārklājumu, kas izgatavots no retām metāliem.",
        requrment: {
            txt: ["Veikt", "Nedariet neko"],
            req: [
                {system: "", metal: "-1", colonist: "", chemicals: "-1", repair: {system: "E", procents: "10"} },
                {system: "", metal: "", colonist: "", chemicals: "", repair: {system: "", procents: ""} }
            ]
        }
    },

    {
        txt: "Uguns kreisajā dzinējā!",
        requrment: {
            txt: ["Nosūtīt personu", "Izlaidiet gaisu", "Lai deg"],
            req: [
                {system: "", metal: "", colonist: "-1", chemicals: "", repair: {system: "G", procents: "10"} },
                {system: "C", metal: "", colonist: "", chemicals: "", repair: {system: "", procents: ""} },
                {system: "", metal: "", colonist: "", chemicals: "", repair: {system: "G", procents: "-10"} }
            ]
        }
    },

    {
        txt: "Jūsu kuģī nav dzīva dvēsele. Kuģis uzsāk pašiznīcināšanas sistēmu un maina savu ceļu uz tuvāko zvaigzni ...",
        requrment: {
            txt: ["Pabeigt"],
            req: [
                {system: "", metal: "", colonist: "", chemicals: "", repair: {system: "", procents: ""} }
            ]
        }
    }

];

var clickebleElements = [];
clickebleElements.push({
    src: document.getElementById("./images/system_a.png").innerHTML,
    src_hover: document.getElementById("./images/system_a_hover.png").innerHTML,
    src_turned: document.getElementById("./images/system_a_turned.png").innerHTML,
    sprite: new Image(),
    x: 582,
    y: 189,
    system: new System("Kapteiņa tilts", 100, "Galvenie sakaru centri atrodas uz tilta. Kad parādās signāls, ir iespējams iegūt noderīgas instrukcijas.", false, "A"),
    systemOpened: false
});

clickebleElements.push({
    src: document.getElementById("./images/system_b.png").innerHTML,
    src_hover: document.getElementById("./images/system_b_hover.png").innerHTML,
    src_turned: document.getElementById("./images/system_b_turned.png").innerHTML,
    sprite: new Image(),
    x: 494,
    y: 159,
    system: new System("Gaisa filtri", 100, "Gaisa filtri savāc lietderīgās gāzes  aiz kukuģa.(ražo 1 vienību ķīmisko vielu)", false, "B"),
    systemOpened: false
});

clickebleElements.push({
    src: document.getElementById("./images/system_c.png").innerHTML,
    src_hover: document.getElementById("./images/system_c_hover.png").innerHTML,
    src_turned: document.getElementById("./images/system_c_turned.png").innerHTML,
    sprite: new Image(),
    x: 455,
    y: 219,
    system: new System("Asteroīdu kolektors", 100, "Aizsargājoša ierīce no dažādām mehāniskām rokām, savāc un no jauna apstrādā asteroīdus, kas lido blakus.(ražo 1 metālu)", false, "C"),
    systemOpened: false
});

clickebleElements.push({
    src: document.getElementById("./images/system_d.png").innerHTML,
    src_hover: document.getElementById("./images/system_d_hover.png").innerHTML,
    src_turned: document.getElementById("./images/system_d_turned.png").innerHTML,
    sprite: new Image(),
    x: 384,
    y: 191,
    system: new System("Medicīniskais nodalījums", 100, "Šajā nodalījumā ir viss, kas jums nepieciešams, lai glābtu cilvēka dzīvību.(ražo 1 veselības vienību)", false, "D"),
    systemOpened: false
});

clickebleElements.push({
    src: document.getElementById("./images/system_e.png").innerHTML,
    src_hover: document.getElementById("./images/system_e_hover.png").innerHTML,
    src_turned: document.getElementById("./images/system_e_turned.png").innerHTML,
    sprite: new Image(),
    x: 220,
    y: 192,
    system: new System("Reaktors", 20, "Kodolreaktors, kas var strādāt bezgalīgi daudz ar atbilstošu apkolpojumu. Remonts ir ļoti grūts, pateicoties reti izmantotajiem metāliem.(Tas ražo 1 vienību enerģijas)", false, "E"),
    systemOpened: false
});

clickebleElements.push({
    src: document.getElementById("./images/system_f1.png").innerHTML,
    src_hover: document.getElementById("./images/system_f1_hover.png").innerHTML,
    src_turned: document.getElementById("./images/system_f1_turned.png").innerHTML,
    sprite: new Image(),
    x: 199,
    y: 326,
    system: new System("Labais dzinējs", 100, "Dzinēji tiek izmantoti manevriem un izkraušanai uz planētas.", false, "F"),
    systemOpened: false
});

clickebleElements.push({
    src: document.getElementById("./images/system_f2.png").innerHTML,
    src_hover: document.getElementById("./images/system_f2_hover.png").innerHTML,
    src_turned: document.getElementById("./images/system_f2_turned.png").innerHTML,
    sprite: new Image(),
    x: 199,
    y: 71,
    system: new System("Kreisais dzinējs", 100, "Dzinēji tiek izmantoti manevriem un izkraušanai uz planētas.", false, "G"),
    systemOpened: false
});