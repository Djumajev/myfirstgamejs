/*
Te ir deklareti visi mainigie kur glabas bildes Base64 formata
Tie mainigiem nem resursu no DOM elementiem
PHP skripts nem visi nepiecisamie bildes, konverte Base64 formata un glaba div elementa ar ID = %path%/fileName
Tas palidz vienu reizi ieladet bildes un vairs neladet vinus

Spele ir izmantota loti interesanta struktura
Kugis ir 800x600 background tekstura kanvasam
Visi citi elemnti ir bildes 800x600 ar caurspidigo fonu
Ir iespejams dinamiski mainit teksturu elementam
*/

var next_turn_src_hover = document.getElementById("./images/next_turn_hover.png").innerHTML;
var music_on = document.getElementById("./images/music_on.png").innerHTML;
var music_on_hover = document.getElementById("./images/music_on_hover.png").innerHTML;
var music_off = document.getElementById("./images/music_off.png").innerHTML;
var music_off_hover = document.getElementById("./images/music_off_hover.png").innerHTML;

var music = new Image();
var next_turn = new Image();
next_turn.src = next_turn_src_hover;

var energy_5 = document.getElementById("./images/energy_5.png").innerHTML;
var energy_4 = document.getElementById("./images/energy_4.png").innerHTML;
var energy_3 = document.getElementById("./images/energy_3.png").innerHTML;
var energy_2 = document.getElementById("./images/energy_2.png").innerHTML;
var energy_1 = document.getElementById("./images/energy_1.png").innerHTML;
var energy_sprite = new Image();

var metal_5 = document.getElementById("./images/metal_5.png").innerHTML;
var metal_4 = document.getElementById("./images/metal_4.png").innerHTML;
var metal_3 = document.getElementById("./images/metal_3.png").innerHTML;
var metal_2 = document.getElementById("./images/metal_2.png").innerHTML;
var metal_1 = document.getElementById("./images/metal_1.png").innerHTML;
var metal_sprite = new Image();

var chemicals_5 = document.getElementById("./images/chemicals_5.png").innerHTML;
var chemicals_4 = document.getElementById("./images/chemicals_4.png").innerHTML;
var chemicals_3 = document.getElementById("./images/chemicals_3.png").innerHTML;
var chemicals_2 = document.getElementById("./images/chemicals_2.png").innerHTML;
var chemicals_1 = document.getElementById("./images/chemicals_1.png").innerHTML;
var chemicals_sprite = new Image();

var colonist_5 = document.getElementById("./images/colonist_5.png").innerHTML;
var colonist_4 = document.getElementById("./images/colonist_4.png").innerHTML;
var colonist_3 = document.getElementById("./images/colonist_3.png").innerHTML;
var colonist_2 = document.getElementById("./images/colonist_2.png").innerHTML;
var colonist_1 = document.getElementById("./images/colonist_1.png").innerHTML;
var colonist_sprite = new Image();

var back = document.getElementById("back").innerHTML;

var txt_back = [document.getElementById("./images/text-box.png").innerHTML,
                document.getElementById("./images/text-box1.png").innerHTML,
                document.getElementById("./images/text-box2.png").innerHTML,
                document.getElementById("./images/text-box3.png").innerHTML,
                document.getElementById("./images/text-box4.png").innerHTML,
                document.getElementById("./images/text-box5.png").innerHTML,
                document.getElementById("./images/text-box6.png").innerHTML,
                document.getElementById("./images/text-box7.png").innerHTML,
                document.getElementById("./images/text-box8.png").innerHTML,
                document.getElementById("./images/text-box9.png").innerHTML,
                document.getElementById("./images/text-box10.png").innerHTML
];

var msg_box_src = document.getElementById("./images/text-box.png").innerHTML;
var switch_on_src = document.getElementById("./images/switch_on.png").innerHTML;
var switch_off_src = document.getElementById("./images/switch_off.png").innerHTML;
var close_src = document.getElementById("./images/close.png").innerHTML;
var close_src_hover = document.getElementById("./images/close_hover.png").innerHTML;