//Globalie mainigie
var canv = document.getElementById("MainScene");
var ctx = canv.getContext("2d");

ctx.font = "20px Gamja Flower";
ctx.fillStyle = "#0F0";

var systemOpened = false;
var turn = 0;
var gameOver = false;

var energy = 1,
    metal = 1,
    colonist = 1,
    chemicals = 1;

//Pause laika summes un ierakstas seit. Pec pause atkal 0
var miliseconds = 0;

//Tas ir pirmais kuru redz speletejs, scenario ir massivs kuru var apskatit scenario.js faila
var msg = new Messages(scenario[0], true);

//Tas ir pats galvena speles funkcija kura notiek bezgaliga rekursija
function GameLoop(){
    
    //izsaucam funkciju kura kontrole muziku
    musicControler();
    //funkcija kura taisa pauzu visa spele
    sleep(miliseconds);

    //tiram ekranu
    ctx.clearRect(0, 0, canv.width, canv.height);

    //dabujam peles poziciju
    canv.addEventListener('mousemove',
        function(evt){
            mousePosition = getMousePos(canv, evt);
        }
    );

    //dabujam peles kliku
    canv.addEventListener('mousedown',
        function(evt){
            clickPosition = getMousePos(canv, evt);
        }
    );

    //izsaucam funkcija, kura kontrole visu speles procesu
    Game();

    //taisam rekursiju
    requestAnimationFrame(GameLoop);
}

function Game(){

    //Tas funkcijas apstrada peles darbu
    systemHover();
    systemClick();

    //rakstam rekordu
    ctx.textAlign="right"; 
    ctx.fillText("Turn: " + turn, 800, 30);

    ctx.textAlign="left"; 
    //izsaucam funkciju, kura kontrole ko zimet
    drawGameElements();

    //ja ir klik, tad spelem skanu
    if(clickPosition.x != null && clickPosition.y != null){
        let click = new sound("./audio/click3.ogg", "click").play();
    }
    
    //onulejam klikus. lai nebutu kludu
    clickPosition.x = null;
    clickPosition.y = null;

    //tiram DOM elementus no klik skanam
    let clicks = document.getElementsByClassName("click");
    for(var i = 0; i < clicks.length; i++)
        if(clicks[i].ended) clicks[i].remove();


    //Parbaudam/ vai cilveks nozuda
    if(colonist <= 0 && !gameOver){
        msg = new Messages(scenario[scenario.length - 1]);
        msg.show == true;
        gameOver = true;
    }
    else if(gameOver && !msg.show){
	msg.show = !msg.show;

    //Ja cilveks nozuda, tad paradresejam vinu uz lapu ar rekordiem
	var form = document.createElement("form");
    	form.setAttribute("method", "POST");
    	form.setAttribute("action", "leaderboard.php");

	var hiddenField = document.createElement("input");
        hiddenField.setAttribute("type", "hidden");
        hiddenField.setAttribute("name", "score");
        hiddenField.setAttribute("value", turn);       
	
	form.appendChild(hiddenField);
	document.body.appendChild(form);
    form.submit();
    }
}


function drawGameElements(){

    //Zimejam visi elementi
    ctx.drawImage(music, 0, 0);

    for (var i = 0; i < clickebleElements.length; i++) {
        ctx.drawImage(clickebleElements[i].sprite, 0, 0);
    }

    for (var i = 0; i < clickebleElements.length; i++) {
        if(clickebleElements[i].systemOpened){
            clickebleElements[i].system.showMSG();
            break;
        }
    }

    uiSticks();

    msg.showMessage();

}

//Realizejam ladesanas ekranu
document.addEventListener(
    "DOMContentLoaded",
    function(){
        document.getElementById("loadingScreen").style.display = "none";
        document.body.style.background = "url(" + back + ")";
        document.body.style.backgroundAttachment = "fixed";
        GameLoop();
    }
);

//pauze spele
function sleep(milliseconds) {
    var start = new Date().getTime();
    while(true) {
      if ((new Date().getTime() - start) > milliseconds){
          miliseconds = 0;
        break;
      }
    }
}

//apstrada noteikumus nakama gajiena
function nextTurn(){
    turn++;

    msg = new Messages(scenario[Math.floor(Math.random() * (scenario.length - 2)) + 1]);
    msg.show = true;

    let energySum = energy;
    for(var i = 0; i < clickebleElements.length; i++)
        if(clickebleElements[i].system.isTurned) energySum++;

    for(var i = 0; i < clickebleElements.length; i++){
        clickebleElements[i].systemOpened = false;

        if(clickebleElements[i].system.myName == "C" && clickebleElements[i].system.isTurned == true && metal < 5)
            metal++;

        if(clickebleElements[i].system.myName == "D" && clickebleElements[i].system.isTurned && colonist < 5)
            colonist++;

        if(clickebleElements[i].system.myName == "E" && clickebleElements[i].system.isTurned && energySum < 5)
            energy++;

        if(clickebleElements[i].system.myName == "B" && clickebleElements[i].system.isTurned && chemicals < 5)
            chemicals++;

        if(clickebleElements[i].system.isTurned){
            if(clickebleElements[i].system.status >= 10)
                clickebleElements[i].system.status -= 10;
    
            if(clickebleElements[i].system.status <= 0){
                clickebleElements[i].system.isTurned = false;
                energy++;
            }
    
            if(clickebleElements[i].system.myName == "E" && clickebleElements[i].system.status <= 0){
                for(var ii = 0; ii < clickebleElements.length; ii++)
                    clickebleElements[i].system.isTurned = false;
                energy = 0;
                energySum = 0;
            }
    
        }
    }

}

//Palidz funkcija
function uiSticks(){
    switch(energy){
        case 0:
            energy_sprite.src = "";
            break;
        case 1:
            energy_sprite.src = energy_1;
            break;
        case 2:
            energy_sprite.src = energy_2;
            break;
        case 3:
            energy_sprite.src = energy_3;
            break;
        case 4:
            energy_sprite.src = energy_4;
            break;
        case 5:
            energy_sprite.src = energy_5;
            break;
    }
    ctx.drawImage(energy_sprite, 0, 0);

    switch(metal){
        case 0:
            metal_sprite.src = "";
            break;
        case 1:
            metal_sprite.src = metal_1;
            break;
        case 2:
            metal_sprite.src = metal_2;
            break;
        case 3:
            metal_sprite.src = metal_3;
            break;
        case 4:
            metal_sprite.src = metal_4;
            break;
        case 5:
            metal_sprite.src = metal_5;
            break;
    }
    ctx.drawImage(metal_sprite, 0, 0);

    switch(colonist){
        case 0:
            colonist_sprite.src = "";
            break;
        case 1:
            colonist_sprite.src = colonist_1;
            break;
        case 2:
            colonist_sprite.src = colonist_2;
            break;
        case 3:
            colonist_sprite.src = colonist_3;
            break;
        case 4:
            colonist_sprite.src = colonist_4;
            break;
        case 5:
            colonist_sprite.src = colonist_5;
            break;
    }
    ctx.drawImage(colonist_sprite, 0, 0);

    switch(chemicals){
        case 0:
            chemicals_sprite.src = "";
            break;
        case 1:
            chemicals_sprite.src = chemicals_1;
            break;
        case 2:
            chemicals_sprite.src = chemicals_2;
            break;
        case 3:
            chemicals_sprite.src = chemicals_3;
            break;
        case 4:
            chemicals_sprite.src = chemicals_4;
            break;
        case 5:
            chemicals_sprite.src = chemicals_5;
            break;
    }
    ctx.drawImage(chemicals_sprite, 0, 0);

}

//Funkcija nem rindkopu un zime to tekstu ar enteriem atbilstosi parametriem
function wrapText(text, x, y, maxWidth, lineHeight){
    var words = text.split(" ");
    var countWords = words.length;
    var line = "";
    for (var n = 0; n < countWords; n++) {
        var testLine = line + words[n] + " ";
        var testWidth = ctx.measureText(testLine).width;
        if (testWidth > maxWidth) {
            ctx.fillText(line, x, y);
            line = words[n] + " ";
            y += lineHeight;
        }
        else {
            line = testLine;
        }
    }
    ctx.fillText(line, x, y);
}
