//Saja klase radam jauno neteikumu un apstradam cilveku izvele
class Messages{

    constructor(msg, isFirst = false){
        this.txt = msg.txt;
        this.requrment = [];
        this.choose = [];
        this.show = true;

        this.isFirst = isFirst;
        this.changeBack = true;
        this.backIndex = 0;

        //Ielikam visie pieprasijumi massiva
        for(var i = 0; i < msg.requrment.txt.length; i++){
            this.requrment.push(msg.requrment.req[i]);
            this.choose.push(msg.requrment.txt[i]);
        }
    }

    //Radam notikumu
    showMessage(){
        //Tas parbaude ir svarigs, lai varetu atri radit un neradit noteikumu ne atbrivojot atminu
        if(this.show){
            this.msg_back = new Image();
            if(this.changeBack && !this.isFirst){
                this.backIndex = Math.floor(Math.random() * txt_back.length);
                this.changeBack = false;
            }
            else if(this.changeBack && this.isFirst){
                this.backIndex = 0;
                this.changeBack = false;
            }
            this.msg_back.src = txt_back[this.backIndex];
            ctx.drawImage(this.msg_back, 0, 0);
    
            wrapText(this.txt, 20, 60, 780, 25);
            for(var i = 0; i < this.choose.length; i++){
                
                let x = 20,
                    y = 150 + 30 * i;

                let strSize = this.choose[i].length * 30;

                if(mousePosition.x > x && mousePosition.x < 800  && mousePosition.y > y - 15 && mousePosition.y < y + 15)
                    ctx.fillStyle = "#050";
                else ctx.fillStyle = "#0F0";

                //Radam kas ir nepieciesams izvelei
                if(this.requrment[i] && (this.requrment[i].system !== "" || this.requrment[i].metal !== "" || this.requrment[i].colonist !== "" || this.requrment[i].chemicals !== "" || this.requrment[i].repair.system !== ""))
                    ctx.fillText(i+1 + ". " + this.choose[i] + "(" +
                                    ((this.requrment[i].system !== "") ? systemTxtW + this.requrment[i].system + turnedOn : "") +
                                    ((this.requrment[i].metal !== "") ? metalTxt + this.requrment[i].metal + "; " : "") + 
                                    ((this.requrment[i].colonist !== "") ? healthTxt + this.requrment[i].colonist + "; " : "") +
                                    ((this.requrment[i].chemicals !== "") ? chemicalsTxt + this.requrment[i].chemicals + "; " : "") +  
                                    ((this.requrment[i].repair.system !== "") ? systemTxtW + this.requrment[i].repair.system + ": " + this.requrment[i].repair.procents + "%;" : "") +
                                ")", x, y);
                else ctx.fillText(i+1 + ". " + this.choose[i], x, y);

                ctx.fillStyle = "#0F0";

                //Parbaudam vai cilveks nospieza uz vienu no izvelam
                if(clickPosition.x > x && clickPosition.x < 800  && clickPosition.y > y - 15 && clickPosition.y < y + 15){
                    let requrments = 0;
                    let ok = 0;

                    //Parbaudam, vai ir viss nepicisams izvelei, otradi radam tekstu ka kaut kas trukst
                    if(this.requrment[i].system !== ""){
                        requrments++;
                        for(var sys = 0; sys < clickebleElements.length; sys++)
                            if(clickebleElements[sys].system.isTurned && clickebleElements[sys].system.myName == this.requrment[i].system)
                                ok++;
                    }
                   
                    if(this.requrment[i].repair.system !== ""){
                        requrments++;
                        this.requrment[i].repair.procents = Number(this.requrment[i].repair.procents);
                        for(var sys = 0; sys < clickebleElements.length; sys++)
                            if(clickebleElements[sys].system.myName === this.requrment[i].repair.system){
                                if(clickebleElements[sys].system.status + this.requrment[i].repair.procents >= 0)
                                    ok++;
                                break;
                            }
                    }

                    if(this.requrment[i].metal !== ""){
                        metal = Number(metal);
                        this.requrment[i].metal = Number(this.requrment[i].metal);

                        requrments++;
                        if(metal + this.requrment[i].metal >= 0){
                            ok++;
                        }
                    }

                    if(this.requrment[i].colonist !== ""){
                        colonist = Number(colonist);
                        this.requrment[i].colonist = Number(this.requrment[i].colonist);

                        requrments++;
                        if(colonist + this.requrment[i].colonist >= 0){
                            ok++;
                        }
                    }

                    if(this.requrment[i].chemicals !== ""){
                        chemicals = Number(chemicals);
                        this.requrment[i].chemicals = Number(this.requrment[i].chemicals);
                        
                        requrments++;
                        if(chemicals + this.requrment[i].chemicals >= 0){
                            ok++;
                        }
                    }

                    if(ok == requrments){
                        chemicals = (chemicals + this.requrment[i].chemicals > 5)? 5 : chemicals + this.requrment[i].chemicals;
                        colonist = (colonist + this.requrment[i].colonist > 5)? 5 : colonist + this.requrment[i].colonist;
                        metal = (metal + this.requrment[i].metal > 5)? 5 : metal + this.requrment[i].metal;

                        for(var sys = 0; sys < clickebleElements.length; sys++)
                            if(clickebleElements[sys].system.myName === this.requrment[i].repair.system){
                                clickebleElements[sys].system.status = (clickebleElements[sys].system.status + this.requrment[i].repair.procents <= 100) ? clickebleElements[sys].system.status + this.requrment[i].repair.procents : 100;
                                break;
                            }
                        this.show = false;
                    } 
                    else{
                        ctx.font = "40px Gamja Flower";
                        ctx.fillStyle = "#F00";
                        ctx.fillText(conditionFalse, 70, 350);
                        miliseconds += 500;
            
                        ctx.font = "20px Gamja Flower";
                        ctx.fillStyle = "#0F0";
                    }

                    if(this.requrment[i].system === "" && this.requrment[i].metal === "" && this.requrment[i].colonist === "" && this.requrment[i].chemicals === "" && this.requrment[i].repair.system === "")
                        this.show = false;
                }
            }

        }
        
    }
}