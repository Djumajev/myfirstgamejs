<!DOCTYPE html>
<head>

    <meta name="viewport" content="width=1287">
    <title>Space Crew</title>

    <link rel="stylesheet" href="../style/style.css">
    <link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">

</head>

<body id="body">

<div style = "background-color: #000; width: 100vw; height: 100vh; z-index: 100; position: fixed; top: 0;" id=loadingScreen>
    <img src="./images/loading.gif" style="margin: auto; right: 0; position: fixed;"> <!-- https://loading.io/#_=_ -->
        <h1 id=msg>Pagrieziet tālruni!</h1>
    <h1 style="color: #FFF; width: 50%; margin: auto; position: fixed; bottom: 20%; left: 40%; ">Ielādē...</h1>
</div>

<?php

    function insert_base64_encoded($img, $name){
        $imageSize = getimagesize($img);
        $imageData = base64_encode(file_get_contents($img));
        $imageSrc = "<div style=\"display: none;\" id='$name'> data:{$imageSize['mime']};base64,{$imageData} </div>";
        echo $imageSrc;
    }

    $backs = array();

    foreach (glob("./images/nebula/*.png") as $filename)
    {
        array_push($backs, $filename);
    }

    $rand_key = array_rand($backs, 1);
    insert_base64_encoded( $backs[$rand_key], "back");

?>

<stars>
    <canvas id="Stars" width=100vw height=100vh>Error :( </canvas>
</stars>

<h1 id=msg>Pagrieziet tālruni!</h1>

<?php

$score = '';

if(isset($_POST['score']))
    $score = $_POST['score'];

?>

<div style="width: 30%; margin: auto; color: #FFF; text-align: center; margin-top: 10%; background-color: #00FFAA11; padding: 2%; border-radius: 10%;">
    <h2>Top spēlētāji</h2>

     <?php

require_once 'db_conf.php';

if($score != ''){
    echo "<h1>Jūsu rekords: ".$score."</h1>";
}

$con = new mysqli(DB_SERVER, DB_USER, DB_PASSWORD, DB_DATABASE);
$con->query("SET CHARSET UTF-8");
    
$result = $con->query("SELECT * FROM score ORDER BY Score DESC LIMIT 10;");
                        
if($result){
    while($row = $result->fetch_assoc()){
        echo "<h3>".$row['Nickname'].": ".$row['Score']."</h3>";
    }
}   

if($score != ''){
    echo "<form method=POST>
            <input type=text name=nick placeholder=\"Jūsu vārds, kapteinis!\" style=\"width: 300px;\"><br>
            <input style='display: none;' name=score value=$score>
            <button name=save><span style=\"font-size: 80%;\">Publicēt</span></button>
        </form>";
}
?>

    <form action="index.php">
        <button><span style=\"font-size: 80%;\">Mājas</span></button>
    </form>

</div>  

<?php

if(isset($_POST['nick'])){
    $con->query("INSERT INTO score(Nickname, Score) VALUES(\"".$_POST['nick']."\", ".$score.");");
    $con->close();
    echo "<script>window.location = \"index.php\"</script>";
} else $con->close();
?>

<script>

  var back = document.getElementById("back").innerHTML;

</script>

<script src="starField.js"></script>

<script>
    document.addEventListener(
        "DOMContentLoaded",
        function(){
            document.getElementById("loadingScreen").style.display = "none";
            document.body.style.background = "url(" + back + ")";
            document.body.style.backgroundAttachment = "fixed";
        }
    );
</script>

</body>
