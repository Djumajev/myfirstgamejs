//Definejam visi audio resursi
var songs = [
    "./audio/Blind Shift.mp3",
    "./audio/Billy's Sacrifice.mp3",
    "./audio/Checking Manifest.mp3",
    "./audio/chippy music 2.wav",
    "./audio/Chippy Music 12.wav",
    "./audio/Chippy Music 16.wav",
    "./audio/Crash Landing.mp3",
    "./audio/DOS-88 - Automatav2.mp3",
    "./audio/DOS-88 - City Stomper.mp3",
    "./audio/DOS-88 - Far away.mp3",
    "./audio/Parabola.mp3",
    "./audio/Race to Mars.mp3",
    "./audio/Smooth Sailing.mp3",
    "./audio/Space Boss Battle 1.mp3"
];

var songIndex = 0;
var musicOn = false;

//Definejam klase kurs taisa DOM elementu html5 audio un palidz stradat ar to
class sound{
        constructor(src, DOMclass) {
        this.sound = document.createElement("audio");
        this.sound.classList.add(DOMclass);
        this.sound.src = src;
        this.sound.setAttribute("controls", "none");
        this.sound.style.display = "none";

        document.body.appendChild(this.sound);
    }

    play(){
        this.sound.play();
    }

    stop(){
        this.sound.pause();
    }

    load(){
        this.sound.load();
    }
}

var player = new sound(songs[Math.floor(Math.random() * songs.length)], "music");

//Funkcija kontrole fon muziku un izvelas vienu dziesmu no massiva songs
function musicControler(){

    if(musicOn){

        if(player.sound.currentTime >= player.sound.duration - 1){
            songIndex = Math.floor(Math.random() * songs.length);

            player.sound.src = songs[songIndex];

            player.load();
            player.play();
        }

        player.play();
    }
    else if(!musicOn)
        player.stop();
        
}