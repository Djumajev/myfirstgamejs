<!DOCTYPE html>
<head>

    <meta name="viewport" content="width=1287">
    <title>Space Crew</title>

    <link rel="stylesheet" href="../style/style.css">
    <link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">

</head>

    <link href="https://fonts.googleapis.com/css?family=Gamja+Flower" rel="stylesheet">

</head>

<body id="body">

<div style = "background-color: #000; width: 100vw; height: 100vh; z-index: 100; position: fixed; top: 0;" id=loadingScreen>
    <img src="./images/loading.gif" style="margin: auto; right: 0; position: fixed;"> <!-- https://loading.io/#_=_ -->
        <h1 id=msg>Pagrieziet tālruni!</h1>
    <h1 style="color: #FFF; width: 50%; margin: auto; position: fixed; bottom: 20%; left: 40%; ">Ielādē...</h1>
</div>

<?php

    function insert_base64_encoded($img, $name){
        $imageSize = getimagesize($img);
        $imageData = base64_encode(file_get_contents($img));
        $imageSrc = "<div style=\"display: none;\" id='$name'> data:{$imageSize['mime']};base64,{$imageData} </div>";
        echo $imageSrc;
    }

    $backs = array();

    foreach (glob("./images/nebula/*.png") as $filename)
    {
        array_push($backs, $filename);
    }

    $rand_key = array_rand($backs, 1);
    insert_base64_encoded( $backs[$rand_key], "back");

?>

<stars>
    <canvas id="Stars" width=100vw height=100vh>Error :( </canvas>
</stars>

<h1 id=msg>Pagrieziet tālruni!</h1>

<div style="width: 50%; margin: auto; color: #FFF; text-align: center; margin-top: 10%; background-color: #00FFAA11; padding: 2%; border-radius: 0%;">
    <h2>Обучение игре</h2>

    <p>
        Laipni lūdzam akadēmijā, kadets! <br> <br>
        Šeit jūs iemācīsities kontrolēt kosmosa kuģi un kļūt par īstu profesionālu tēju dzeršanā.<br>
        Sāksim ar vienkāršu. Tas ir tavs kuģis.
        <img src="./Tutorial/dictor 5.png" width=100%>
        Lai paaugstinātu patoso un cieņu pret pārējo apkalpi, varat ieslēgt mūziku augšējā kreisajā stūrī.
        <img src="./Tutorial/dictor 8.png" width=100%>
        Uz kuģa ir sistēmas. Katrai sistēmai ir konkrēts uzdevums, varat droši uzklikšķināt uz tiem, lai iegūtu detalizētu sistēmas aprakstu un noskaidrotu tā pašreizējo stāvokli.
        Lai ieslēgtu sistēmu, jums ir jāpiespiež lielais sarkans slēdzis, ja viss ir veiksmīgs - tas kļūs zaļš. Sistēmas ar statusu 0% nevar ieslēgt un 1 enerģijas vienība ir nepieciešama, lai darbinātu sistēmu.
        Ieslegta sistēma zaudē 10% sava stāvokļa katru gajienu, jo jūsu komanda ir sertificētas makakas.
        <img src="./Tutorial/dictor 10.png" width=100%> 
        <img src="./Tutorial/dictor 12.png" width=100%>
        Dažas sistēmas papildina resursus vienā kārtas laikā, arī jebkura sistēma var būt nepieciešama nākamajā kārtā laikā izlases gadījumā. Faktiski jūsu resursi:
        <ul style="text-align: left;">
        <li>Enerģija ir vajadzīga, lai nodrošinātu sistēmas<br><label style="font-size: 80%;">(Ja reaktoru saplīst - enerģija pazūd)</label></li>
        <li>Ķimikālijas un metāli - var būt noderīgi pilnīgi neparedzamās situācijās.</li>
        <li>Apkalpes veselība - vissvarīgākais resurss, kad tas beidzas, jūs zaudējat</li>
        </ul> 
        <img src="./Tutorial/dictor 17.png" width=100%>
        Kad resursu planošana, sistēmas ieslegšana un tēja ir gatavi, noklikšķiniet uz pogas "nākamais solis"
        <img src="./Tutorial/dictor 19.png" width=100%>
        Kursa sākumā notiek viens RANDOM notikums, kas var gan pozitīvi ietekmēt lidojumu, gan nogalināt visu apkalpi. Pasākums sastāv no paša pasākuma un tā risinājumiem, kurus jūs izvēlaties.
        Risinājuma nosacījums (ja tāds ir) ir ierakstīts iekavās, un, ja šie nosacījumi ir izpildīti, jūs varat izvēlēties šo piedzīvojumu attīstības variantu.
        <img src="./Tutorial/dictor 26.png" width=100%>
        Spēle turpinās tik ilgi, kamēr jūsu komanda dzīvo, spēles mērķis ir iegūt visaugstāko iespējamo rekordu (padarīt pēc iespējas vairāk gajienus), pēc jūsu apkalpes nāves jūs varat publicēt savu rezultātu
        un, ja viņš ir labs, tad jūs būsiet tabulā ar labākajiem spēlētājiem.<br>
        <h3>Veiksmi!</h3>
    </p>

    <form action="index.php">
        <button><span style=\"font-size: 80%;\">Mājas</span></button>
    </form>
    
</div>  

<script>

  var back = document.getElementById("back").innerHTML;

</script>

<script src="starField.js"></script>

<script>
    document.addEventListener(
        "DOMContentLoaded",
        function(){
            document.getElementById("loadingScreen").style.display = "none";
            document.body.style.background = "url(" + back + ")";
            document.body.style.backgroundAttachment = "fixed";
        }
    );
</script>

</body>