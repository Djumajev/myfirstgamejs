//Globals tekst kurs tik izmantots spele
var noEnergy = "Не хватает энергии!",
systemTxt = "Система: ",
systemTxtW = "Система ",
conditionFalse = "Условия не соблюдены",
statusTxt = "Состояние: ",
turnedOn = ": Вкл.; ",
metalTxt = "Металл: ",
healthTxt = "Здоровье: ",
chemicalsTxt = "Химикаты: ";

//Noteikumi kuri var notikt ar speleteju
//Svarigi ka pirmais - pitmais kuru cilveks redzes
//Pedejais - kad cilveks zaude
//Ja nevajag radit resursu rakstam "", ja bus tuks, var gaidit kludu
//txt massiva rakstam tekstu izvelem
//req massiva kas nepieciesams
//Svarigi! ja txt massiva tekst bus 1 vieta, kas nepiecisams ari bus 1 vieta massiva req
//Ludzu rakstitet tik daudz req, cik ir txt massiva elementus
//Drikst izmantot "" tuksas iekavas
var scenario = [
    {
        txt: "Добро пожаловать на борт, Капитан. Нажмите ниже, что бы продолжить.",
        requrment: {
            txt: ["Нажмите меня"],
            req: [
                {system: "", metal: "", colonist: "", chemicals: "", repair: {system: "", procents: ""} }
            ]
        }
    },

    {
        txt: "Недалеко от газового гиганта вы находите старый спутник.",
        requrment: {
            txt: ["Ничего не делать", "Разобрать на детали", "Использовать как комутатор"],
            req: [
                {system: "", metal: "", colonist: "", chemicals: "", repair: {system: "", procents: ""} },
                {system: "C", metal: "1", colonist: "", chemicals: "", repair: {system: "", procents: ""} },
                {system: "A", metal: "", colonist: "1", chemicals: "", repair: {system: "", procents: ""} }
            ]
        }
    },

    {
        txt: "Ваши радары не заметили астероид, который несётся прямо на вас.",
        requrment: {
            txt: ["Ничего не делать", "Уклониться", "Уклониться", "Переработать"],
            req: [
                {system: "", metal: "", colonist: "-1", chemicals: "", repair: {system: "", procents: ""} },
                {system: "G", metal: "", colonist: "", chemicals: "", repair: {system: "", procents: ""} },
                {system: "F", metal: "", colonist: "", chemicals: "", repair: {system: "", procents: ""} },
                {system: "C", metal: "1", colonist: "", chemicals: "", repair: {system: "C", procents: "-10"} }
            ]
        }
    },

    {
        txt: "Вы попали в солнечную вспышку.",
        requrment: {
            txt: ["Ничего не делать", "Быстро покинуть зону"],
            req: [
                {system: "", metal: "", colonist: "-1", chemicals: "", repair: {system: "", procents: ""} },
                {system: "E", metal: "", colonist: "", chemicals: "", repair: {system: "E", procents: "-10"} }
            ]
        }
    },


    {
        txt: "Ваш корабль не спеша подлетает к станции MK-1298. Судя по архиву она давно заброшена и вы решаете разобрать её, что бы починить...",
        requrment: {
            txt: ["Лететь дальше", "Починить батареи", "Починить воздужные фильтры", "Починить левый двигатель", "Ничего"],
            req: [
                {system: "", metal: "", colonist: "", chemicals: "", repair: {system: "", procents: ""} },
                {system: "C", metal: "", colonist: "", chemicals: "", repair: {system: "E", procents: "10"} },
                {system: "C", metal: "", colonist: "", chemicals: "", repair: {system: "B", procents: "10"} },
                {system: "C", metal: "", colonist: "", chemicals: "", repair: {system: "G", procents: "10"} },
                {system: "C", metal: "1", colonist: "", chemicals: "", repair: {system: "", procents: ""} }
            ]
        }
    },

    {
        txt: "В этой системе обсолютно ничего нет, вы заводите движки и готовитесь к киберпрыжку.",
        requrment: {
            txt: ["Улететь"],
            req: [
                {system: "", metal: "", colonist: "", chemicals: "", repair: {system: "", procents: ""} }
            ]
        }
    },

    {
        txt: "Сенсоры засекли планету с пригодной атмосферой.",
        requrment: {
            txt: ["Лететь дальше", "Поискать полезные ресурсы", "Поискать полезные ресурсы"],
            req: [
                {system: "", metal: "", colonist: "", chemicals: "", repair: {system: "", procents: ""} },
                {system: "G", metal: "1", colonist: "", chemicals: "1", repair: {system: "", procents: ""} },
                {system: "F", metal: "1", colonist: "", chemicals: "1", repair: {system: "", procents: ""} }
            ]
        }
    },

    {
        txt: "Один из членов вашего экипажа вышел в открытый космос, что бы провести починку, но его трос оборвался.",
        requrment: {
            txt: ["Спасти", "Оставить"],
            req: [
                {system: "E", metal: "", colonist: "", chemicals: "", repair: {system: "A", procents: "10"} },
                {system: "", metal: "", colonist: "-1", chemicals: "", repair: {system: "", procents: ""} }
            ]
        }
    },

    {
        txt: "В столовой сломался стол, экипаж начинает бунтовать.",
        requrment: {
            txt: ["Произвести новый стол", "Выкинуть главаря бунтущих", "Сделать бочку, что бы всех тошнило", "Сделать мёртвую петлю, что бы всех тошнило"],
            req: [
                {system: "", metal: "", colonist: "", chemicals: "-1", repair: {system: "", procents: ""} },
                {system: "", metal: "", colonist: "-1", chemicals: "", repair: {system: "", procents: ""} },
                {system: "G", metal: "", colonist: "", chemicals: "", repair: {system: "", procents: ""} },
                {system: "F", metal: "", colonist: "", chemicals: "", repair: {system: "", procents: ""} }
            ]
        }
    },

    {
        txt: "Ионая буря в системе угрожает всей електронике на корабле.",
        requrment: {
            txt: ["Улететь как можно скорее", "Улететь как можно скорее", "Ничего не делать"],
            req: [
                {system: "F", metal: "", colonist: "", chemicals: "", repair: {system: "", procents: ""} },
                {system: "G", metal: "", colonist: "", chemicals: "", repair: {system: "", procents: ""} },
                {system: "", metal: "", colonist: "-1", chemicals: "", repair: {system: "", procents: ""} }
            ]
        }
    },

    {
        txt: "Гидропонная ферма вышла из строя, люди голодают.",
        requrment: {
            txt: ["Пускай голодают", "Синтезировать еду", "Починить ферму"],
            req: [
                {system: "", metal: "", colonist: "-1", chemicals: "", repair: {system: "", procents: ""} },
                {system: "", metal: "", colonist: "", chemicals: "-1", repair: {system: "", procents: ""} },
                {system: "C", metal: "", colonist: "", chemicals: "", repair: {system: "", procents: ""} }
            ]
        }
    },

    {
        txt: "Инженеры предлагают заклеить протекающюю трубу изолентой.",
        requrment: {
            txt: ["Согласиться", "Отказаться"],
            req: [
                {system: "", metal: "", colonist: "", chemicals: "-1", repair: {system: "G", procents: "10"} },
                {system: "", metal: "", colonist: "", chemicals: "", repair: {system: "", procents: ""} }
            ]
        }
    },

    {
        txt: "В системе есть редкая аномалия, котрая при взоимодействие с металлами воспроизводит энергию. Это могло бы нам помочь выключить реактор на ремонт без потери електричества",
        requrment: {
            txt: ["Ничего не делать", "Выкинуть металл"],
            req: [
                {system: "", metal: "", colonist: "", chemicals: "", repair: {system: "", procents: ""} },
                {system: "", metal: "-1", colonist: "", chemicals: "", repair: {system: "E", procents: "10"} }
            ]
        }
    },

    {
        txt: "Экипаж загорелся идей что-то починить.",
        requrment: {
            txt: ["Починить мостик", "Починить мед. отсек", "Починить фильтры", "Приказать ничего не делать"],
            req: [
                {system: "", metal: "-1", colonist: "", chemicals: "", repair: {system: "A", procents: "10"} },
                {system: "", metal: "", colonist: "", chemicals: "-1", repair: {system: "D", procents: "10"} },
                {system: "", metal: "-1", colonist: "", chemicals: "-1", repair: {system: "B", procents: "10"} },
                {system: "", metal: "", colonist: "", chemicals: "", repair: {system: "", procents: ""} }
            ]
        }
    },

    {
        txt: "Экипаж загорелся идей что-то починить.",
        requrment: {
            txt: ["Левый двигатель", "Правый двигатель", "Реактор", "Приказать ничего не делать"],
            req: [
                {system: "", metal: "-1", colonist: "", chemicals: "", repair: {system: "G", procents: "10"} },
                {system: "", metal: "", colonist: "", chemicals: "-1", repair: {system: "F", procents: "10"} },
                {system: "", metal: "-1", colonist: "", chemicals: "-1", repair: {system: "E", procents: "10"} },
                {system: "", metal: "", colonist: "", chemicals: "", repair: {system: "", procents: ""} }
            ]
        }
    },

    {
        txt: "Очень горячяя звезда в системе позволяет расплавить некоторые наши ресурсы, что бы обновить обшивку реактора, сделанную из редких метталов.",
        requrment: {
            txt: ["Сделать", "Ничего не делать"],
            req: [
                {system: "", metal: "-1", colonist: "", chemicals: "-1", repair: {system: "E", procents: "10"} },
                {system: "", metal: "", colonist: "", chemicals: "", repair: {system: "", procents: ""} }
            ]
        }
    },

    {
        txt: "Пожар в левом двигателе!",
        requrment: {
            txt: ["Отправить человека", "Выпустить воздух", "Пускай горит"],
            req: [
                {system: "", metal: "", colonist: "-1", chemicals: "", repair: {system: "G", procents: "10"} },
                {system: "C", metal: "", colonist: "", chemicals: "", repair: {system: "", procents: ""} },
                {system: "", metal: "", colonist: "", chemicals: "", repair: {system: "G", procents: "-10"} }
            ]
        }
    },

    {
        txt: "На вашем корабле не осталось ни одной живой души. Корабль запускает систему самоуничтожения и меняет свой курс на ближайшую звезду...",
        requrment: {
            txt: ["Закончить"],
            req: [
                {system: "", metal: "", colonist: "", chemicals: "", repair: {system: "", procents: ""} }
            ]
        }
    }

];

var clickebleElements = [];
clickebleElements.push({
    src: document.getElementById("./images/system_a.png").innerHTML,
    src_hover: document.getElementById("./images/system_a_hover.png").innerHTML,
    src_turned: document.getElementById("./images/system_a_turned.png").innerHTML,
    sprite: new Image(),
    x: 582,
    y: 189,
    system: new System("Капитанский мостик", 100, "На капитанском мостике расположены основные узлы связи. При появлении сигнала возможно получить полезные инструкции.", false, "A"),
    systemOpened: false
});

clickebleElements.push({
    src: document.getElementById("./images/system_b.png").innerHTML,
    src_hover: document.getElementById("./images/system_b_hover.png").innerHTML,
    src_turned: document.getElementById("./images/system_b_turned.png").innerHTML,
    sprite: new Image(),
    x: 494,
    y: 159,
    system: new System("Воздушные фильтры", 100, "Воздушные фильтры собирают полезные газы за короблём.(прозводит 1 едицу химикатов)", false, "B"),
    systemOpened: false
});

clickebleElements.push({
    src: document.getElementById("./images/system_c.png").innerHTML,
    src_hover: document.getElementById("./images/system_c_hover.png").innerHTML,
    src_turned: document.getElementById("./images/system_c_turned.png").innerHTML,
    sprite: new Image(),
    x: 455,
    y: 219,
    system: new System("Собиратель астероидов", 100, "Хитрое приспособление из множества механических рук, собирает и переобрабатывает летящие мимо астероиды.(производит 1 металл)", false, "C"),
    systemOpened: false
});

clickebleElements.push({
    src: document.getElementById("./images/system_d.png").innerHTML,
    src_hover: document.getElementById("./images/system_d_hover.png").innerHTML,
    src_turned: document.getElementById("./images/system_d_turned.png").innerHTML,
    sprite: new Image(),
    x: 384,
    y: 191,
    system: new System("Мед. отсек", 100, "В этом отсеке есть всё необходимое, что бы спасти человеческую жизнь.(производит 1 единицу здоровия)", false, "D"),
    systemOpened: false
});

clickebleElements.push({
    src: document.getElementById("./images/system_e.png").innerHTML,
    src_hover: document.getElementById("./images/system_e_hover.png").innerHTML,
    src_turned: document.getElementById("./images/system_e_turned.png").innerHTML,
    sprite: new Image(),
    x: 220,
    y: 192,
    system: new System("Реактор", 20, "Атомный реактор, способный проработать целую вечность при должном содержании. Ремонт крайне сложен из-за редких используемых металлов.(прозводит 1 едицу енергии)", false, "E"),
    systemOpened: false
});

clickebleElements.push({
    src: document.getElementById("./images/system_f1.png").innerHTML,
    src_hover: document.getElementById("./images/system_f1_hover.png").innerHTML,
    src_turned: document.getElementById("./images/system_f1_turned.png").innerHTML,
    sprite: new Image(),
    x: 199,
    y: 326,
    system: new System("Правый двигатель", 100, "Двигатели служат для манёвров и посадок на планеты.", false, "F"),
    systemOpened: false
});

clickebleElements.push({
    src: document.getElementById("./images/system_f2.png").innerHTML,
    src_hover: document.getElementById("./images/system_f2_hover.png").innerHTML,
    src_turned: document.getElementById("./images/system_f2_turned.png").innerHTML,
    sprite: new Image(),
    x: 199,
    y: 71,
    system: new System("Левый двигатель", 100, "Двигатели служат для манёвров и посадок на планеты.", false, "G"),
    systemOpened: false
});